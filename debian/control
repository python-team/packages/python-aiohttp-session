Source: python-aiohttp-session
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Martin <debacle@debian.org>
Build-Depends: debhelper-compat (= 13),
	       dh-python,
	       pybuild-plugin-pyproject,
	       python3-aiohttp (>= 3.10),
	       python3-aiomcache,
	       python3-all,
	       python3-attr,
	       python3-chardet,
	       python3-cryptography,
	       python3-docker,
	       python3-flake8,
	       python3-multidict,
	       python3-nacl,
	       python3-pytest,
	       python3-pytest-aiohttp,
	       python3-pytest-cov,
	       python3-pytest-mock,
	       python3-pytest-sugar,
	       python3-redis,
	       python3-setuptools,
	       python3-sphinx,
	       python3-yarl,
Standards-Version: 4.1.5
Rules-Requires-Root: no
Homepage: https://github.com/aio-libs/aiohttp-session
Vcs-Git: https://salsa.debian.org/python-team/packages/python-aiohttp-session.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-aiohttp-session
Testsuite: autopkgtest-pkg-pybuild

Package: python3-aiohttp-session
Architecture: all
Depends: ${python3:Depends},
	 ${misc:Depends}
Description: provides sessions for aiohttp.web
 The library allows us to store user-specific data into a session object.
 .
 The session object has a dict-like interface (operations like
 session[key] = value, value = session[key] etc. are present).
 .
 Before processing the session in a web-handler, you have to register the
 session middleware in aiohttp.web.Application.

Package: python-aiohttp-session-doc
Section: doc
Architecture: all
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Multi-Arch: foreign
Description: provides sessions for aiohttp.web (documentation)
 The library allows us to store user-specific data into a session object.
 .
 The session object has a dict-like interface (operations like
 session[key] = value, value = session[key] etc. are present).
 .
 Before processing the session in a web-handler, you have to register the
 session middleware in aiohttp.web.Application.
 .
 This package provides the HTML documentation.
